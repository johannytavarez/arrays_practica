﻿using System;

namespace arrays_practica
{
    class array
    {
        /*Un programa que pida al usuario
         4 números, los memorice (utilizando un array), calcule su media aritmética 
         y después muestre en pantalla la media y los datos tecleados.*/
         
        static void Main(string[] args)
        {
            
            Console.WriteLine("Bienvenido, ingrese 4 números");
            int [] media_aritmetica = new int[4];

            media_aritmetica [0]= Convert.ToInt32(Console.ReadLine());
            media_aritmetica [1]= Convert.ToInt32(Console.ReadLine());
            media_aritmetica [2]= Convert.ToInt32(Console.ReadLine());
            media_aritmetica [3]= Convert.ToInt32(Console.ReadLine());

            //string cadena1 = "56782469";
            double suma = 0.0;
            for (int i = 0; i < media_aritmetica.Length; i++)
            {
                suma += media_aritmetica[i];
            }
            double resultado = suma / media_aritmetica.Length;

            Console.WriteLine("La media aritmetica de los numeros ingresados es {0}", resultado);
            Console.WriteLine("Los números ingresados fueron: ");

            for (int c = 0; c < media_aritmetica.Length; c++){
                Console.WriteLine(media_aritmetica[c]);
            }

            
            
        }
    }
}
