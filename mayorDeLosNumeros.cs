﻿using System;
using System.Linq;

namespace Mayor {
    class Program{
        static void Main(string[] args){

            //Un programa que pida al usuario 10 números y luego calcule y muestre cuál es el mayor de todos ellos

            Console.WriteLine("Ingrese 10 numeros");
            int [] numMayor = new int [10];

            for (int i = 0; i<10; i++){
                numMayor [i] = int.Parse(Console.ReadLine());

            }


            Console.WriteLine("El mayor numero ingresado es: " + numMayor.Max());
           
        

        }
    }
}
