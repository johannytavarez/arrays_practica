﻿using System;

namespace Practica_arrays {
    class Meses{
        static void Main(string[] args){

                /*Un programa que almacene en un array el número de días que tiene cada mes (supondremos que es un año no bisiesto), pida 
                al usuario que le indique un mes (1=enero, 12=diciembre) y muestre en pantalla el número de días que tiene ese mes.*/
        
            byte [] dias= {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
            string [] Mes = {"", "Enero","Febrero", "Marzo", "Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
            byte mes;


            Console.WriteLine("Ingrese el numero que representa a un mes y se le dira el número de dias que tiene");
            mes = byte.Parse(Console.ReadLine());

            if (mes<= 12 && mes > 0)
            {
                Console.WriteLine("El mes de {0} tiene {1} dias", Mes[mes] ,dias[mes]);
            }

            else
            {
                Console.WriteLine("Ingrese un numero de mes correcto, recordando que enero = 1, febrero = 2, etc...");
            }




        }
    }
}
