﻿using System;

namespace reales {
    class Program{
        static void Main(string[] args){
        /*Un programa que pida al usuario 5 números reales (pista: necesitarás 
        un array de "float") y luego los muestre en el orden contrario al que se introdujeron.*/

        Console.WriteLine("Ingrese 5 numeros reales");
        float[] numReales = new float [5];
        numReales[0] = float.Parse(Console.ReadLine());
        numReales[1] = float.Parse(Console.ReadLine());
        numReales[2] = float.Parse(Console.ReadLine());
        numReales[3] = float.Parse(Console.ReadLine());
        numReales[4] = float.Parse(Console.ReadLine());

        Console.WriteLine("Los numeros que usted ingreso, al contrario son: ");
        for (int i = 0; i<5; i++){
            Console.WriteLine(numReales[5- i-1]+ "\t");

        }

        }
    }
}
