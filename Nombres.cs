﻿using System;

namespace Nombres {
    class Program{
        static void Main(string[] args){

            /*Un programa que prepare espacio para un máximo de 100 nombres. El usuario deberá ir introduciendo un nombre cada vez,
             hasta que se pulse Intro sin teclear nada, momento en el que dejarán de pedirse más nombres y se mostrará en pantalla la 
             lista de los nombres que se han introducido.*/
             
             Console.WriteLine("Ingrese nombres");
             Console.WriteLine("Cuando termine de escribir los nombres pulse Enter");

             string [] nombres= new string [100];

             
             for (int i = 0; i < 100; i++){
             nombres [i]= Console.ReadLine();

             if (nombres [i] == ""){
                 break;
             }

             }

             Console.Clear();

             Console.WriteLine("Los nombres ingresados fueron: ");
             for(int x = 0; x<100; x++){
                 Console.WriteLine(nombres[x]);

                if (nombres [x] == ""){
                 break;
             } 


             }

        }
    }
}
