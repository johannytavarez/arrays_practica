﻿using System;

namespace Practica_arrays {
    class Valores{
        static void Main(string[] args){

                //Crear una clase que permita ingresar valores enteros por teclado y nos muestre la tabla de multiplicar de dicho valor. Finalizar el programa al ingresar el -1.
             Console.WriteLine("Se generara una tabla de multiplicar de el numero que ingrese. Para salir ingrese -1");
             int i, entrada;
                do {
                Console.WriteLine("Ingrese un numero");
                
                entrada = int.Parse(Console.ReadLine());
            
                for(i=0;i<13;i++){
                Console.WriteLine("{0} * {1} = {2}", i, entrada, (i * entrada));

                }

              } while (entrada != -1);



        }
    }
}
